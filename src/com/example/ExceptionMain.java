package com.example;

public class ExceptionMain {
    /* errors and exceptions both are derived from Throwable class
        Exceptions are also derived from Throwable class
        They are having 2 sub types init
        1. checked Exception
        2. Unchecked Exception
        Errors are occured due to syntatical mistakes
        1. spelling wrong --Public
        2. bracket mistakes --
        If the error occurs then we want to able execute the program
        Exceptions are errors which can be handled
        Exceptions are occurred due to wrong inputs given by the programmer or given by the user
        The point where exception is occurred after that the program will not get executed if it is
        not handled
        Exceptions can be handled by try, catch also by throw and throws

     */
    public static void main(String[] args) {
        Arrays arrays = new Arrays();
        try {
            arrays.input();
            arrays.display();
           // arrays.calLength();
        }
        catch (NullPointerException exception){
            System.out.println("NullPointer Exception");
        }
        catch (Exception exception)//generic cath block which used for handling all type of exceptions
        {
            System.out.println(exception.getMessage());
        }
        finally {
            System.out.println("Important Call");
        }
    }
}
