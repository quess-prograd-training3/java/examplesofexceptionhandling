package com.example;

public class Arrays2Main {
    public static void main(String[] args) {
        Arrays2 arrays2 = new Arrays2();
        try {
            arrays2.input();
            arrays2.display();
            arrays2.calculate();
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
}
